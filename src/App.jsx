import React from 'react';
import {BrowserRouter} from "react-router-dom";
import {Switch} from "react-router-dom";
import {Route} from "react-router-dom";
import {NavigationBar} from "../src/NavigationBar"
import {HomePage} from "./HomePage";
import {ContactPage} from "./ContactPage";
import {CONTACT_PAGE} from "./Constants";
import {HOME_PAGE} from "./Constants";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles({

    navigate:{

        backgroundColor:'#5dbdb5',

    },

    footer:{

        backgroundColor: '#5dbdb5',
        paddingLeft:'0.5vw',
        paddingTop:'0.5vh',
        paddingBottom:'0.5vh',

    }
})

export default function App() {

    const classes = useStyles()

  return (

      <BrowserRouter>
          <div>
              <div className={classes.navigate}>
                  <NavigationBar />
              </div>

              <Switch>

                  <Route path={CONTACT_PAGE}>
                      <ContactPage />
                  </Route>

                  <Route path={HOME_PAGE}>
                      <HomePage />
                  </Route>

              </Switch>

              <p className={classes.footer}>Copyright &copy; 2020 Yuhao_Liu</p>

          </div>

    </BrowserRouter>

  );
}

