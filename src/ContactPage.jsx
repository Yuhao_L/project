import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Button} from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";

const useStyles = makeStyles({

    contactpage:{

        minHeight:'85vh',

    },

    textarea:{

        minWidth:'90%',
        maxWidth:'90%',
        minHeight:'50vh',
        maxHeight:'50vh'
    },

    button:{
        variant:"contained",
        color:'primary'
    }

})

export function ContactPage(){

    const classes = useStyles();

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };


    return(
        <div className={classes.contactpage}>
            <h1>This is Contact page</h1>
            <textarea className={classes.textarea}/><br/>
            <Button variant="contained" color="primary" onClick={handleClickOpen}>
                Submit
            </Button>
            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
                <DialogContent dividers>
                        Your Submitted successfully.
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleClose} color="primary">
                        Close
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}