import {useHistory} from "react-router-dom"
import Button from '@material-ui/core/Button'
import React from "react";
import {CONTACT_PAGE, HOME_PAGE} from "./Constants";
import Toolbar from "@material-ui/core/Toolbar";

export function NavigationBar(){

    const history = useHistory();

    function Home(){

        history.push(HOME_PAGE);

    }

    function Contact(){

        history.push(CONTACT_PAGE);
    }

    return(

        <Toolbar position="static">

            <Button
                color="inherit"
                onClick={Home}>

                Home
            </Button>

            <Button
                color="inherit"
                onClick={Contact}>

                Contact
            </Button>

        </Toolbar>

    )

}
