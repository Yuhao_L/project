import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles({

    homepage:{

        minHeight:'85vh',

    }

})

export function HomePage(){

    const classes = useStyles();

    return(
        <div className={classes.homepage}>
            <h1>Yuhao Liu</h1>
            <h2>Information</h2>
            <p>
                <h5>Phone:<br/>+1 (203) 640-1476</h5>
                <h5>Email:<br/>yliu12@unh.newhaven.edu</h5>
                <h5>Address:<br/>1 Atwood PL, Unit 414<br/>
                West Haven, CT<br/>
                    06156</h5>
            </p>
            <h2>Looking For:</h2>
            <p>Full Time Job</p>
            <h2>Project</h2>
            <h5>Python Matplotlib</h5>
            <p>
                This application can make user use a text file to save the data in name and numbers.<br/>
                For example, student grades in different quiz and exam.<br/>
                Then read them and display them in different graphs as user want.<br/>
                Bar graph can show each quiz and exam grade for one people or all people.<br/>
                And the pie chart can show percent of people who get A, B or C in each quiz and exam.<br/>
                Histogram can show the distribution of people in each quiz and exam.<br/>
                So, this app can easy help professor or analyst to see an intuitional data.<br/>
            </p>
        </div>

    )
}